package puzzle;


import java.util.ArrayList;

class Permutation
{
	public void checkPermutation(char[] c,int start,int end)
	{
		int res=0;
		if(start==end)
		{
			//System.out.println(String.valueOf(c));
			  System.out.println(checkPelindrome(String.valueOf(c)));		//checking the pelindrome. with each permutation string.
 
		} 
			for(int i=start;i<c.length;i++)
				{
					swap(c,start,i);
					checkPermutation(c, start+1, end);
					swap(c,start,i);
				}		 
	}
	public void swap(char[] c,int i,int j)
	{		 
		char temp=c[i];
		c[i]=c[j];
		c[j]=temp;
	}
	
	public boolean checkPelindrome(String p)
	{
 
		String t="";boolean status=false;
		for(int i=p.length()-1;i>=0;i--)
		{
			t=t+p.charAt(i);			
		}
		if(t.equals(p))
		{
		//	System.out.println("Original String is: "+p +"\nReverse Of String: "+t +" Both are Equal");
			return true;
		}
		else
		{
		 return false;
		}
		
	}
}
public class Day4Puzzle4 {

	public static void main(String[] args) {
	 
		String s="ABA";
		Permutation p=new Permutation();
		 p.checkPermutation(s.toCharArray(),0,s.length()-1);	

	}

}

