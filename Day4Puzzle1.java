package puzzle;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import java.sql.Timestamp;

class HitCounter
{
	ArrayList al=new ArrayList();
	public void hit(int t)								//whenever we call hit() it will take the system time. nd stored it arraylist
	{
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	    long time=timestamp.getTime()+t;
	
		al.add(time);
 		// System.out.println(time);			
	}
	public void get()											//get will give the total number of hits.
	{
		int count=0;
		for(int i=0;i<al.size();i++)
		{
			count++;
		}
		System.out.println("Total hits: "+count);
		 
	}
	public void getRange(int lower,int higher)						//it will display d time bw range.
	{
		System.out.println("Number of hit's from "+lower+" to "+higher);
		for(int i=lower;i<higher;i++)
		{
			System.out.println(i+") "+al.get(i));
		}
	}
}
public class Day4Puzzle1 {

	public static void main(String[] args) {
	
		HitCounter hc=new HitCounter();
		hc.hit(1);
		hc.hit(2);
		hc.hit(3);
		//hc.get();
		hc.hit(4);
		hc.get();
		
		hc.getRange(2,4);
		
//		Scanner sc=new Scanner(System.in);		
//		while(true) {
//			System.out.println("Press 1 to Hit: ");
//			System.out.println("Press 2 to Get: ");
//			System.out.println("Press 3 to Range: ");
//			System.out.println("Press 4 to Exit: ");
//			System.out.println("--------");
//			int ch=sc.nextInt();
//			switch(ch)
//			{
//			case 1:hc.hit();
//				break;
//			case 2:hc.get();
//				break;
//			case 3:hc.getRange();
//				break;
//			case 4:
//				break;
//				default:System.out.println("Please Enter correct Number");
//					
//				
//			}
			
//		}
		
		

	}

}
