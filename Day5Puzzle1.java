package Day5;

import java.util.Collection;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


class Flattendict
{
	String mainkey="";
	LinkedHashMap<String, Object> newmap=new LinkedHashMap<String, Object>();
	public Map check(Map lhm,String s )
	{
//		System.out.println(lhm);
		//input :{key=3, 
//					foo={
//						a=5, 
//						bar={
//							baz=8
//							}
//						}
//					}
		
		String str=s;
		lhm.forEach((key,value)->{
			String newkey=(String) key;
			//System.out.println(key+":"+value);
			if(value instanceof Integer)
			{
				newkey=str+"-"+newkey;
				//System.out.println(newkey+"="+value+newkey.length());
				newmap.put(newkey.substring(1),value );
			}
			else
			{ 
				if(value instanceof Object)
				{
					mainkey=mainkey+"-"+(String) key;
					//System.out.println(key);
					check((Map) value, mainkey);
				}
			}
			 
		});
		return newmap;	
//		System.out.println(newmap); 
	}	 
}

public class Day5Puzzle1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 
		 Map<String, Object> map1=new LinkedHashMap<String, Object>();
		 Map<String, Object> map2=new LinkedHashMap<String, Object>();
		 Map<String, Object> map3=new LinkedHashMap<String, Object>();
		map1.put("baz", 8);
		map2.put("a", 5);
		map2.put("bar", map1);
		map3.put("key",3);
		map3.put("foo", map2);
		
		Flattendict fd=new Flattendict();
		Map res=fd.check(map3,"");
		System.out.println(res);
		 
		
	}	 
}
