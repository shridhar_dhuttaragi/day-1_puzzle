package puzzle;

import java.util.ArrayList;
import java.util.Collections;

class Node
{
	int data;
	Node left,right=null;
	Node(int data)
	{
		this.data=data;
		this.left=null;
		this.right=null;
	}
}
class BinaryTree
{
	public Node insert(int data)
	{
		Node node=new Node(data);
		return node;
	}
	ArrayList al=new ArrayList<>();
	public ArrayList minpathSum(Node root,int i,int[] a)
	{
		if(root==null)
		{
			return al ;
		}
		a[i]=root.data;
		if(root.left==null && root.right==null)
		{
			al=findMin(a,i);
		}
		minpathSum(root.left,i+1,a);
		minpathSum(root.right, i+1, a);
		return al;
	}
	
	public ArrayList findMin(int a[],int i)
	{
		int sum=0;int min=a[0];
		for(int j=0;j<=i;j++)
		{
			sum+=a[j];
		}
		//System.out.println(sum);
		al.add(sum);
		return al;	
	}
 

}

public class Day4Puzzle3 {
	public static void main(String args[])
	{
		
		BinaryTree bt=new BinaryTree();
		Node root=new Node(10);
		root.left=bt.insert(5);
		root.right=bt.insert(5);
		root.left.right=bt.insert(2);
		root.right.right=bt.insert(1);
		root.right.right.left=bt.insert(-1);
//		bt.display(root);
		int a[]=new int[10];
		ArrayList al= bt.minpathSum(root,0,a);
		System.out.println(Collections.min(al));
		
		
		
	}
}
