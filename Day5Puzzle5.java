package Day5;

import java.util.ArrayList;
import java.util.Arrays;

class SumofSubarray
{
	public int check(int a[])
	{
		ArrayList al=new ArrayList();
		int index=0;
		 for(int i=0;i<a.length;i++)								// removing duplicate value from d array.
		 {
			 int flag=0;
			 for(int j=0;j<i;j++)
			 {
				 if(a[i]==a[j])
				 {
					 flag=1;
					 break;
				 }
			 }
			 if(flag==0)
			 {
				al.add(a[i]);
			 }
		 }
//		 for(int i=0;i<al.size();i++)
			return al.size();	
	}
}
public class Day5Puzzle5 {

	public static void main(String[] args) {
		int a[]= {5,1,3,5,2,3,4,1,1};
		
		Arrays.sort(a);
		SumofSubarray sum=new SumofSubarray();
		System.out.println("Size "+sum.check(a));

	}

}
