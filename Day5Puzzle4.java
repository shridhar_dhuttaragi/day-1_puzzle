package Day5;

class Node
{
	int data;
	Node next;
	Node(int data)
	{
		this.data=data;
		this.next=null;
	}
}
class Rotation
{
	Node head=null;
	Node tail=null;
	public void insert(int data)
	{
		Node newNode=new Node(data);
		if(head==null)
		{
			head=newNode;
			tail=newNode;
		}
		else
		{
			tail.next=newNode;
			tail=newNode;
		}			
	}
	
	public void rotate(int key)
	{
		Node tail=head;
		int len=0;
		while(tail.next!=null)
		{
			tail=tail.next;
			len++;
		}
//		System.out.println(len);
		tail.next=head;
		Node cur=head;
		for(int i=1;i<key;i++)
		{
			cur=cur.next;
		}
		head=cur.next;
		cur.next=null;
		 
	}
	public void display()
	{
		Node temp=head;
		while(temp!=null)
		{
			System.out.print(temp.data+" ");
			temp=temp.next;
		}
		System.out.println();
	}
	
}
public class Day5Puzzle4 {

	public static void main(String[] args) {

		int a[]= {7,7,3,5};
		int key=2;
		
		Rotation r=new Rotation();
		
		for(int i=0;i<a.length;i++)
		{
			r.insert(a[i]);
		}
		System.out.println("Before Rotation:");
		r.display();
		r.rotate(key);
		System.out.println("After Rotation:");
		r.display();

	}

}
