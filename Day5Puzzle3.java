package Day5;

import java.util.HashMap;

class Mapping
{
	public boolean check(String s1,String s2)
	{
		if(s1.length()!=s2.length())
		{
			return false;
		}
		HashMap hm=new HashMap<>();
		for(int i=0;i<s1.length();i++)
		{
			char ss1=s1.charAt(i);
			char ss2=s2.charAt(i);
			
			if(hm.containsKey(ss1))						//take d frst char check tht char is present in hashmap.not put.
			{
				if((char)hm.get(ss1)!=ss2) {				 
					return false;
				}				 
			}else 
			{
				hm.put(ss1, ss2);		
			}
		}
		return true;
	}
}
public class Day5Puzzle3 {

	public static void main(String[] args) {
			
		Mapping m=new Mapping();
		String s1="foo";
		String s2="daa";
		System.out.println(m.check(s1,s2));		

	}

}
